package db.migration;

import java.sql.Connection;

import com.googlecode.flyway.core.api.migration.jdbc.JdbcMigration;

public class V3__Java_Migration_With_FlyWay implements JdbcMigration {

	public void migrate(Connection connection) throws Exception {
		// do whatever you want wit the db connection 
		System.out.println("V3__Java_Migration_With_FlyWay.migrate()");
	}
}
