package com.example.flyway;


import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ServiceMain {
	public static void main(String[] args) {
		BeanFactory beanFactory = new ClassPathXmlApplicationContext("spring.xml");
		
		SomeService s = beanFactory.getBean(SomeService.class);
		s.doSomething();
		
	}
}
