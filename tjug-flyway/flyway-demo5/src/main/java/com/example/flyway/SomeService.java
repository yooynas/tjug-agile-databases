package com.example.flyway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class SomeService {
	
	@Autowired SomeRepository repository;
	
	public void doSomething	()
	{
		System.out.println("SomeService.doSomething()");
		this.repository.getSomething();
	}
}
