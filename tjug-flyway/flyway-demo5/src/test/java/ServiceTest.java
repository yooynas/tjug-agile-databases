import javax.sql.DataSource;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.example.flyway.SomeService;
import com.googlecode.flyway.test.annotation.FlywayTest;
import com.googlecode.flyway.test.junit.FlywayTestExecutionListener;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring.xml" })
@TestExecutionListeners({   
          DependencyInjectionTestExecutionListener.class,
          FlywayTestExecutionListener.class })
@FlywayTest
public class ServiceTest {

	@Autowired
	private SomeService service;

	@Autowired 
	private DataSource ds; 
	
	@Test
	@FlywayTest(invokeCleanDB=true,locationsForMigrate={"db/test"})
	public void testOne()
	{
		System.out.println("ServiceTest.testOne()");
		JdbcTemplate template  = new JdbcTemplate(ds);
		long count = template.queryForLong("SELECT count(*) from demo5.user_accounts");
		Assert.assertEquals(1, count);
	}
	
	@Test
	@FlywayTest(invokeCleanDB=true,locationsForMigrate={"db/test"})
	public void testTwo()
	{
		System.out.println("ServiceTest.testTwo()");
	}
}
